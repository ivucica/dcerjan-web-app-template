module.exports = function(grunt) {
  // List all project folder names here
  const projects = [ "app" ];

  const path = { libs: "node_modules" };

  const libs = [
    {name: "almond", path: `${path.libs}/almond/almond`},
    {name: "react", path: `${path.libs}/react/dist/react.min`},
    {name: "react-dom", path: `${path.libs}/react-dom/dist/react-dom.min`},
    {name: "socket.io", path: `${path.libs}/socket.io-client/socket.io`}
  ];

  const excludeVendorLibs = libs.map(lib => lib.name);
  const tasksToLoad = ["grunt-contrib-watch", "grunt-sass", "grunt-eslint", "grunt-babel", "grunt-mocha-phantomjs", "grunt-contrib-requirejs"];

  tasksToLoad.forEach(task => grunt.loadNpmTasks(task));

  let config = {
    watch: { options: { spawn: false, livereload: true } },
    eslint: {},
    sass: { options: { outputStyle: "compressed" } },
    babel: { options: { sourceMap: "inline"} },
    mocha_phantomjs: { options: { reporter: "spec", config: { useColors: true} } },
    requirejs: { options: { optimize: "none", paths: {}, packages: [] } }
  };

  projects.forEach( project => {
    // Configure stylesheet watcher
    config.watch[`scss/${project}#scss`] = { files: [`source/client/${project}/scss/**/*.scss`], tasks: [`sass:${project}#scss`] };
    // Configure node-sass
    config.sass[`${project}#scss`] = { files: { [`assets/build/${project}/style.css`]: `source/client/${project}/scss/style.scss`} };

    // Configure javascript watcher
    config.watch[`${project}#js`] = { files: [`source/client/${project}/js/**/*.js`], tasks: [`eslint:${project}#js`, `babel:${project}#js`, `mocha_phantomjs:${project}#test`] };
    // Configure eslint
    config.eslint[`${project}#js`] = [`source/client/${project}/js/**/*.js`];
    // Configure babel
    config.babel[`${project}#js`] = { files: [{cwd: `source/client/${project}/js`, expand: true, src: [`**/*.js`], dest: `assets/build/${project}/js`, ext: ".js"}] };
    // Configure mocha-phantomjs
    config.mocha_phantomjs[`${project}#test`] = [`source/client/${project}/unit.html`];
    // Configure requirejs packages && build targets
    config.requirejs.options.packages.push({ name: project, location: `assets/build/${project}/js` });
    config.requirejs[project] = { options: { include: [project], exclude: excludeVendorLibs, insertRequire: [project], out: `assets/build/${project}/index.js` } }
  });
  // Configure requirejs lib paths
  libs.forEach(lib => { config.requirejs.options.paths[lib.name] = lib.path });
  // Configure requirejs vendor target
  config.requirejs.vendor = { options: { include: excludeVendorLibs, out: `assets/build/vendor.js`} };

  // Init grunt configuration
  grunt.initConfig(config);

  // Default task is watch task - runs all watchers
  grunt.registerTask("default", ["watch"]);
  // Test task - runs all tests
  grunt.registerTask("test", ["mocha_phantomjs"]);

  // Build tasks
  grunt.registerTask("warmup", ["sass", "eslint", "babel", "mocha_phantomjs"]);
  // Dev build (unminified)
  grunt.registerTask("dev-build", ["sass", "babel", "mocha_phantomjs", "requirejs"]);
  // Release build (minified)
  grunt.registerTask("prepare-release-build", () => { grunt.config.set("requirejs.options.optimize", "uglify2"); grunt.config.set("requirejs.options.preserveLicenseComments", true); });
  grunt.registerTask("release-build", ["prepare-release-build", "sass", "babel", "mocha_phantomjs", "requirejs"]);
};