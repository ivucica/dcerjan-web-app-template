"use strict";

let
  config = require("../config.js"),
  mongoose = require("mongoose"),
  express = require("express"),
  winston = require("winston"),
  io = require("socket.io"),
  morgan = require("morgan"),
  cookie = require("cookie-parser"),
  body = require("body-parser");


// config mongoose
mongoose.Promise = global.Promise;
if (config.db.url) {
  mongoose.connect(config.db.url);
} else {
  winston.warn("Mongo database connection string not specified in config");
}


let
  app = express();

app.use(morgan("dev"));
app.use(cookie());
app.use(body.json());
app.use(body.urlencoded({ extended: true }));

//app.use(express.static(__dirname + "/../static"));
if (process.env.NODE_ENV !== "production") {
  winston.info(__dirname);
  app.use("/node_modules", express.static(__dirname + "/../../node_modules"));
  app.use("/assets", express.static(__dirname + "/../../assets"));
}

app.set("view engine", "ejs");
app.set("views", __dirname + "/views");

require("./routes")(app);

let run = (port) => {
  let server = app.listen(port, () => {
    require("./socket")(server);

    let
      host = server.address().address,
      port = server.address().port;
    winston.info(`Sedna server listening at '${host}:${port}'`);
  });
}

module.exports = {run};
