const path = {
  lib: "../../../node_modules",
  build: "../../../assets/build"
};

const libs = [
  {name: "chai", path: `${path.lib}/chai/chai`},
  {name: "react", path: `${path.lib}/react/dist/react`},
  {name: "react-dom", path: `${path.lib}/react-dom/dist/react-dom`},
  {name: "socket.io", path: `${path.lib}/socket.io-client/socket.io`}
];

let config = { baseUrl: "./", paths: {} };
libs.forEach(lib => config.paths[lib.name] = lib.path);
requirejs.config(config);


define("test", ["chai"], function (chai) {
  "use strict";

  window.expect = chai.expect;
  window.assert = chai.assert;

  mocha.setup("bdd");

  requirejs([
    `${path.build}/app/js/tests/dummyTest.js`
  ], () => mocha.run());

});

// run the tests
requirejs(["test"]);
