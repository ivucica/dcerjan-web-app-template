import socket from "./socket";

let callbacks = [];

function recieve(message) {
    callbacks.forEach(cb => cb(message));
}

function send(channel, message) {
    socket.emit(channel, message);
}

socket.on("message", (message) => {
    recieve(message);
});

socket.on("connected", (message) => {
    recieve({timestamp: Date.now(), message: JSON.stringify(message)});
});

export default {
    subscribe: (cb) => {
        callbacks.push(cb);
    },
    publish: (message) => {
        send("message", message);
    }
};
