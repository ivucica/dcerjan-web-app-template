# Web app template

The following are notes written by a newbie, for a newbie.

## Install newer Node.js

    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
	sudo apt-get install -y nodejs
	npm install
	nodejs source/server/main.js

## Start Grunt

### Optional: homedir-based npms and grunt

    echo prefix=~/.npm > ~/.npmrc
	export PATH=${HOME}/.npm/bin:$PATH

### Install and start grunt

In a new term:

	npm install -g grunt
	grunt

## Development

For development work visit <http://localhost:8080/dev>

To have stuff appear, start editing files. Save a SCSS file once, or save a
JS file once.

Alternatively, warm up and then fire up grunt watch:

	grunt warmup
	grunt

### Making a build

To see a build at <http://localhost:8080/>:

	grunt dev-build

To cut a minified release:

	grunt release-build

